Name: ocp-sso-token
Version: 1.2.0
Release: %autorelease
Summary: Non-interactive CLI login for OpenShift via OIDC/Kerberos

License: GPL-3.0-or-later
URL: https://gitlab.com/cki-project/ocp-sso-token

Source0: https://gitlab.com/cki-project/ocp-sso-token/-/archive/v%{version}/ocp-sso-token-v%{version}.tar.gz

BuildArch: noarch
BuildRequires: python3-devel

%description
Non-interactive CLI login for OpenShift clusters via OpenID-connected IdP with Kerberos support

%prep
%autosetup

%generate_buildrequires
%pyproject_buildrequires -e spec

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files ocp_sso_token

%check
%tox

%files  -f %{pyproject_files}
%license LICENSE
%doc README.md LICENSE
%{_bindir}/ocp-sso-token

%changelog
%autochangelog
