"""Test helpers."""
import contextlib
from importlib import resources
import typing

import responses

from tests import assets


@contextlib.contextmanager
def setup_responses(forward: bool = False) -> typing.Generator[responses.RequestsMock, None, None]:
    """Add response mocks."""
    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
        rsps.add(
            responses.GET, 'https://api.cluster:6443/.well-known/oauth-authorization-server',
            body=resources.read_binary(assets, 'well_known_oauth_authorization_server.json'),
            content_type="application/json")
        rsps.add(
            responses.GET, 'https://oauth-openshift.apps.cluster/oauth/token/request',
            headers={'Location': 'https://oauth-openshift.apps.cluster/oauth/authorize?'
                     'client_id=openshift-browser-client&redirect_uri=https%3A%2F%2F'
                     'oauth-openshift.apps.cluster%2Foauth%2Ftoken%2Fdisplay&response_type=code'},
            status=302)
        if forward:
            rsps.add(
                responses.GET, 'https://oauth-openshift.apps.cluster/oauth/authorize?'
                'client_id=openshift-browser-client&redirect_uri=https%3A%2F%2F'
                'oauth-openshift.apps.cluster%2Foauth%2Ftoken%2Fdisplay&response_type=code',
                headers={'Location': 'https://auth/openid-connect/auth?client_id=https%3A%2F%2F'
                         'cluster&redirect_uri=https%3A%2F%2Foauth-openshift.apps.cluster%2F'
                         'oauth2callback%2FOpenID&response_type=code&scope=email+openid+profile'},
                status=302)
        else:
            rsps.add(
                responses.GET, 'https://oauth-openshift.apps.cluster/oauth/authorize?'
                'client_id=openshift-browser-client&redirect_uri=https%3A%2F%2F'
                'oauth-openshift.apps.cluster%2Foauth%2Ftoken%2Fdisplay&response_type=code',
                body=resources.read_binary(assets, 'authorize_list.html'),
                status=200)
            rsps.add(
                responses.GET, 'https://oauth-openshift.apps.cluster/oauth/authorize?'
                'client_id=openshift-browser-client&idp=OpenID&redirect_uri=https%3A%2F%2F'
                'oauth-openshift.apps.cluster%2Foauth%2Ftoken%2Fdisplay&response_type=code',
                headers={'Location': 'https://auth/openid-connect/auth?client_id=https%3A%2F%2F'
                         'cluster&redirect_uri=https%3A%2F%2Foauth-openshift.apps.cluster%2F'
                         'oauth2callback%2FOpenID&response_type=code&scope=email+openid+profile'},
                status=302)
        rsps.add(
            responses.GET, 'https://auth/openid-connect/auth?client_id=https%3A%2F%2F'
            'cluster&redirect_uri=https%3A%2F%2Foauth-openshift.apps.cluster%2F'
            'oauth2callback%2FOpenID&response_type=code&scope=email+openid+profile',
            headers={'Location': 'https://oauth-openshift.apps.cluster/oauth2callback/OpenID'},
            status=302)
        rsps.add(
            responses.GET, 'https://oauth-openshift.apps.cluster/oauth2callback/OpenID',
            headers={'Location': '/oauth/authorize?client_id=openshift-browser-client&'
                     'idp=OpenID&redirect_uri=https%3A%2F%2Foauth-openshift.apps.cluster%2F'
                     'oauth%2Ftoken%2Fdisplay&response_type=code'},
            status=302)
        rsps.add(
            responses.GET, 'https://oauth-openshift.apps.cluster/oauth/authorize?'
            'client_id=openshift-browser-client&idp=OpenID&redirect_uri=https%3A%2F%2F'
            'oauth-openshift.apps.cluster%2Foauth%2Ftoken%2Fdisplay&response_type=code',
            headers={'Location': 'https://oauth-openshift.apps.cluster/oauth/token/display?'
                     'code=sha256~code1'},
            status=302)
        rsps.add(
            responses.GET, 'https://oauth-openshift.apps.cluster/oauth/token/display?'
            'code=sha256~code1',
            body=resources.read_binary(assets, 'display_get.html'),
            status=200)
        rsps.add(
            responses.POST, 'https://oauth-openshift.apps.cluster/oauth/token/display',
            body=resources.read_binary(assets, 'display_post.html'),
            status=200)
        yield rsps
